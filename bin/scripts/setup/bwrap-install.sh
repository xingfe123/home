
if [ ! -d "bubblewrap-0.4.1" ]; then
    wget https://github.com/projectatomic/bubblewrap/releases/download/v0.4.1/bubblewrap-0.4.1.tar.xz
    tar -xvf bubblewrap-0.4.1.tar.xz
fi

cd bubblewrap-0.4.1
./configure --prefix=$HOME && make install