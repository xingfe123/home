
git clone -b release https://github.com/roswell/roswell.git --depth=1
cd roswell
sh bootstrap
./configure --prefix=$HOME
make
make install
