

if [ ! -f $HOME/bin/opam  ]; then
    wget https://raw.github.com/ocaml/opam/master/shell/opam_installer.sh
    sh ./opam_installer.sh $HOME/bin
fi
