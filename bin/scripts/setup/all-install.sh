



#env CC=cc CXX=CC
# download cmake-3.17.3.tar.gz

braft-install.sh
bwrap-install.sh
emacs-install.sh
install-clang.sh
install-cmake.sh
install-leveldb.sh
sqlite3-install.sh

# if [ ! -d "leveldb/" ]; then
#    git clone --recurse-submodules https://github.com/google/leveldb.git leveldb/
# fi

# if [ ! -d "leveldb/build" ];then
#     mkdir -p leveldb/build && cd leveldb/build
# fi

# cmake -DCMAKE_BUILD_TYPE=Release .. && cmake --build .
