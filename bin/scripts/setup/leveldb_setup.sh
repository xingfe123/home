
cd ~/build

git clone --depth=1 --recurse-submodules https://github.com.cnpmjs.org/google/leveldb.git
cd leveldb
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release .. && cmake --build .
make
sudo make install
