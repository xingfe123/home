#env CC=cc CXX=CC
# download cmake-3.17.3.tar.gz
prefix=cmake-3.17.3
if [ ! -d $prefix ] ; then
   wget https://www.sqlite.org/snapshot/$prefix.tar.gz
   tar -xvf $prefix.tar.gz
fi

cd $prefix
CC=gcc CXX=g++ ./bootstrap --prefix=$HOME
make -j
make install

# if [ ! -d "leveldb/" ]; then
#    git clone --recurse-submodules https://github.com/google/leveldb.git leveldb/
# fi

# if [ ! -d "leveldb/build" ];then
#     mkdir -p leveldb/build && cd leveldb/build
# fi

# cmake -DCMAKE_BUILD_TYPE=Release .. && cmake --build .