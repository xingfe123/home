function require_rpm(){
    $package=$1
    ret=$(rpm -q $package |grep 'not installed')
    if [ $ret -eq 0 ]; then
        sudo yum install -y $package
    fi
}



function install_openblas(){
    wget https://jaist.dl.sourceforge.net/project/openblas/v0.3.19/OpenBLAS%200.3.19%20version.tar.gz --no-check-certificate
    tar -xvf OpenBLAS\ 0.3.19\ version.tar.gz
    cd xianyi-OpenBLAS-2480e50/
    make FC=gfortran -j
    make PREFIX=$HOME install
}

function install_blas(){
    wget http://www.netlib.org/blas/blas-3.10.0.tgz
    tar -xvf blas-3.10.0.tgz
    cd BLAS-3.10.0/
    make -j
    mv blas_LINUX.a ~/lib/libblas.a
}

function install_armadillo(){
    wget http://sourceforge.net/projects/arma/files/armadillo-10.8.2.tar.xz --no-check-certificate
    tar -xvf armadillo-10.8.2.tar.xz
    cd armadillo-10.8.2
    cmake . -DCMAKE_INSTALL_PREFIX=$HOME
    make install -j
}

function install_sdl2(){
    wget https://www.libsdl.org/release/SDL2-2.0.20.tar.gz --no-check-certificate
    tar -xvf SDL2-2.0.20.tar.gz
    cd SDL2-2.0.20
    mkdir build
    cd build
    ../configure --prefix=$HOME
    C_INCLUDE_PATH=~/include:$C_INCLUDE_PATH make install -j
}

function tinyemu_require(){
    sudo dnf install openssl-devel libcurl-devel SDL-devel
    #It is possible to compile the programs without these libraries by
    # commenting CONFIG_FS_NET and/or CONFIG_SDL in the Makefile.

}
