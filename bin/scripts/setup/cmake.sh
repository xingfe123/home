

wget https://cmake.org/files/v3.18/cmake-3.18.4.tar.gz --no-check-certificate
tar -xvf cmake-3.18.4.tar.gz
cd cmake-3.18.4
./configure --prefix=$HOME
make -j 2 install
