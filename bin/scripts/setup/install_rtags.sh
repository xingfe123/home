sudo yum install llvm-toolset-7-llvm-devel llvm-toolset-7-clang-devel devtoolset-7-gcc-c++
scl enable devtoolset-7 llvm-toolset-7 bash
git clone --recursive https://github.com/Andersbakken/rtags.git
cd rtags
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 .
make install
