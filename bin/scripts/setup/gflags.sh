@version 2.2.1
@package gflags

@tar-url https://github.com/gflags/gflags/archive/v$version.tar.gz
@install(){
    @cmake .. -DCMAKE_INSTALL_PREFIX=$HOME
    @make install 
}
