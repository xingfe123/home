cd $HOME/build


git clone --recurse-submodules -b v1.42.0 https://github.com.cnpmjs.org/grpc/grpc
cd grpc
git submodule update --init
mkdir -p cmake/build
pushd cmake/build
cmake -DgRPC_INSTALL=ON \
      -DgRPC_BUILD_TESTS=OFF \
      -DCMAKE_INSTALL_PREFIX=$HOME ../..

make -j
make install
popd


# prefix=$HOME/local/protobuf

# # Build and install protobuf
# cd ./third_party/protobuf
# ./autogen.sh
# ./configure --prefix=$prefix
# make -j `nproc`
# make install

# # Build and install gRPC
# cd ../..
# make -j `nproc` PROTOC=${prefix}/bin/protoc
# make prefix=$prefix install
