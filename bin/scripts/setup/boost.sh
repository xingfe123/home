@package boost 

@download(){
    git clone https://github.com/boostorg/boost.git 
    cd boost 
    git submodule update --init
    ./bootstrap.sh --prefix=$HOME
    ./b2 headers
}

@install(){
    ./b2 install --prefix=$HOME
}



