package=emacs
version=26.3

# @require gcc

# @tar-url https://mirrors.tuna.tsinghua.edu.cn/gnu/emacs/$package-$version.tar.gz

install(){
    ./configure  --prefix=$HOME --with-x=no --with-xpm=no --with-jpeg=no --with-gif=no --with-tiff=no --with-x-toolkit=no --with-png=no --with-gnutls=no
    make install
}

#sudo yum install gcc gnutls-devel gnutls gnutls-utils -y
#scl enable devtoolset-9 bash 
#-check-certificate
# gnutls-cli --x509cafile /etc/pki/tls/certs/ca-bundle.crt -p 443 melpa.org...failed
wget --no-check-certificate https://mirrors.tuna.tsinghua.edu.cn/gnu/emacs/$package-$version.tar.gz
tar -xvf $package-$version.tar.gz
cd ./$package-$version
install
