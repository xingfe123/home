package=ninja
version=1.10.2
tarUrl=https://codeload.github.com/ninja-build/ninja/tar.gz/refs/tags/v${version}
wget ${tarUrl} -o $package-$version.tar.gz
tar -zxvf $package-$version.tar.gz
cd $package-$version
./configure.py --bootstrap
install ninja $HOME/bin
