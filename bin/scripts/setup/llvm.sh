# @require svn
# @require ninja
# @require gcc 4.8
# @package llvm


function download(){
    #svn co http://llvm.org/svn/llvm-project/llvm/trunk llvm
    git clone --depth 1 https://github.com/llvm/llvm-project.git

    # cd llvm/tools
    # svn co http://llvm.org/svn/llvm-project/cfe/trunk clang
    # svn co http://llvm.org/svn/llvm-project/lld/trunk lld
    # svn co http://llvm.org/svn/llvm-project/polly/trunk polly

    # @cd llvm/tools/clang/tools
    # svn co http://llvm.org/svn/llvm-project/clang-tools-extra/trunk extra

    # @cd llvm/projects

    # svn co http://llvm.org/svn/llvm-project/compiler-rt/trunk compiler-rt
    # svn co http://llvm.org/svn/llvm-project/libcxx/trunk libcxx
    # svn co http://llvm.org/svn/llvm-project/libcxxabi/trunk libcxxabi
}

#cmake -DCMAKE_INSTALL_PREFIX=$HOME/local/llvm -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_CXX_FLAGS="-stdlib=libc++ -std=c++14"
#}

git clone https://github.com/llvm/llvm-project.git
cd llvm-project
mkdir build
cd build
cmake  ../llvm -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra" -DLLVM_BUILD_TESTS=ON  -DCMAKE_BUILD_TYPE=RelWithDebInfo
make check -j 16
make clang-test -j 16

make install clang-tidy -j 16
