package=global
version=6.6.2
tarUrl=http://tamacom.com/global/$package-$version.tar.gz

wget $tarUrl --no-check-certificate
tar -xvf $package-$version.tar.gz
cd $package-$version
./configure --prefix=$HOME
make install -j
