

work_dir="libpcap-1.7.4"

if [! -d "$work_dir" ]; then
    wget http://www.tcpdump.org/release/$work_dir.tar.gz && tar xzf $work_dir.tar.gz
fi

cd $work_dir

./configure --prefix=$HOME && make install
