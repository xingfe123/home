# .bashrc


# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific environment

function install_gcc(){
    sudo yum install -y mariadb-devel
    sudo yum install centos-release-scl -y
    sudo yum install devtoolset-8-gcc devtoolset-8-gcc-c++ -y
    sudo yum install devtoolset-7 llvm-toolset-7.0 -y
    scl enable devtoolset-7 llvm-toolset-7.0 -- bash
}


# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
# sudo yum install -y patch
# Source global definitions

# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
#
# outputting anything in those cases.
#cat .ssh/id_rsa.pub | ssh -p 23068 luoxing@113.6.252.163 'cat >> .ssh/authorized_keys'
#if [[ $- != *i* ]] ; then
#	# Shell is non-interactive.  Be done now!
#	return
#fi
# .bashrc

# Source global definitions
export INCLUDEOS_PREFIX=~/includeos/
export LIBRARY_PATH=$HOME/lib:$LIBRARY_PATH

export PATH="$HOME/.local/bin:$HOME/bin:$PATH:"
export PATH=$PATH:$INCLUDEOS_PREFIX/bin

export LD_LIBRARY_PATH=$HOME/lib/:$HOME/lib64/:$LD_LIBRARY_PATH
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$HOME/lib/pkgconfig/
export C_INCLUE_PATH=$HOME/include/:$C_INCLUDE_PATH
export CPLUS_INCLUE_PATH=$C_INCLUDE_PATH:$CPLUS_INCLUDE_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib64

#golang configure
export GOROOT=~/golang
#export GOPATH=~/go:/usr/local/
#export GOOS=linux
#export GOARCH=amd64
export PATH=~/go/bin:$PATH

export PATH=$PATH:/usr/libexec/git-core
export PATH=~/bin:~/.local/bin/:$CIM_HOME/bin/:~/bin/cargo/bin:$PATH

export PATH=/usr/local/texlive/2018/bin/x86_64-linux:$PATH

export MANPATH=/usr/local/texlive/2018/texmf-dist/doc/man:$MANPATH
export INFOPATH=/usr/local/texlive/2018/texmf-dist/doc/info:$INFOPATH

##############################################################
#sudo yum install npm
mkdir -p $HOME/.npm/
export PATH=$HOME/.npm/bin:$PATH

##############################################################################

##################
#export DISPLAY=:0
export CIM_HOME=~/.cim/

#export LANG=en_US.utf8
################################

#
#export CXX="clang++ -stdlib=libc++ -std=c++14"
#export CC=clang




#################################
alias login-gentoo='ssh 122.204.142.93 -p 22137'
alias lein-repl='lein repl :connect 122.204.142.93:13780'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias cmake='cmake -DCMAKE_INSTALL_PREFIX=$HOME -DCMAKE_CXX_FLAGS=-I$HOME/include'


programIsNotRuning(){
    [ -z "$(ps aux|grep $(whoami)|grep $1|grep -v grep)" ]
}

yesOrNo(){
    while true
    do
        echo -n "Enter yes or no: "
        read x
        case "$x" in
            y | yes ) return 0 ;;
            n | no ) return 1;;
            * ) echo "Answer yes or no";;
        esac
     done
}


ifrestart(){
    sudo ifdown $1
    sudo ifup $1
}

updateKernel(){
    cd /usr/src/linux-4.0.5-gentoo/
    smake oldconfig && smake && smake install
    sudo genkernel --lvm --install initramfs
    sudo grub2-mkconfig -o /boot/grub/grub.cfg
    cd
}

updateWorld(){
    system = $(head -n 1 /etc/issue)
    smerge --sync && smerge -uDN world && smerge @module-rebuild
}

install_pyenv(){
    sh ~/bin/scripts/setup/pyenv.sh
}

function setup_pyenv(){
    # pyenv: no such command 'virtualenv-init'
    export PYENV_VIRTUALENV_DISABLE_PROMPT=1
    export PYENV_ROOT="${HOME}/.pyenv"
    export PATH="${PYENV_ROOT}/bin:${PYENV_ROOT}/shims:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
    export PYENV_VIRTUALENV_DISABLE_PROMPT=1
    pyenv global 3.6.5
}

function when-which(){
    which $1
    if [ $? -eq 0 ]; then
	$2
    fi
}

function unless-which(){
    which $1
    if [ $? -ne 0 ]; then
	$2
    fi
}

function crawl(){
    cd ~/news_spiders/scrapy/scrapy_crawl/bin/$1
}

function crawl-run(){
    cd ~/oscrapy/
    dune exec ./$1.exe
    crawl $1
    scrapy crawl $1
}

function codegen(){
    cd ~/oscrapy/;
    $EDITOR $1.ml
}

function crawl-edit(){
    codegen $1
}

function ready-to-release(){
    cp ../moer/scrapy.cfg .
    cp ../moer/start.py .
    cp ../moer/tar_moer.sh "tar_$1.sh"
    sed -i "s/moer/$1/g" scrapy.cfg
    sed -i "s/moer/$1/g" start.py
    ./start.py
}

function tag-package(){
    git tag $1
    "./tar_$1.sh" *
}


function view-pipelines(){
    crawl $1
    emacs $1/pipelines.py
}

function view-db(){
    crawl $1
    emacs $1/db.py
}

function oinstall(){
    cd ~/ocinstall/
    dune exec ./ocinstall.exe $1
}


function view-pipelines(){
    crawl $1
    emacs $1/pipelines.py
}

function view-items(){
    crawl $1
    emacs $1/items.py
}

function git-archive1(){
    name=$(basename "$PWD")
    TIMETAIL=`date +%Y%m%d`
    git archive --format=tar.gz -o /tmp/$name-$TIMETAIL.tar.gz --prefix=./ luoxing-dev
}

if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

which clang >/dev/null 2>&1
clang_version=$?


currentver="$(gcc -dumpversion)"

requiredver="8"

if [ "$(printf '%s\n' "$requiredver" "$currentver" | sort -V | head -n1)" = "$requiredver" ]; then
          echo "Greater than or equal to 8.0.0"
     export CC=gcc
     export CXX=g++
elif [ $clang_version -eq 0 ]; then
    #export CXX=clang++
    #export CC=clang
    echo "xy"
else
    # install_gcc
    echo "xz"
fi

SHELLY_HOME=$HOME/.shelly;
##############################

[ -s "$SHELLY_HOME/lib/shelly/init.sh" ] && . "$SHELLY_HOME/lib/shelly/init.sh"

if [ $(uname -s) != "Darwin" ] ;then
    alias ls='ls --color=auto'
    alias which='alias | /usr/bin/which'
    if [ -f ~/bin/emacs ];then
	alias emacs='~/bin/emacs --no-windows --debug-init'
    else
	alias emacs='emacs --no-windows --debug-init'
    fi
fi

# emacs --daemon

export PS1='\u@\h \W \$'

function pingCheck(){
    ping -c 1 $1 |grep '100% packet loss';
    if [ $? -eq 0 ]; then
	echo "$1 failed"
    else
	echo "$1 suc"
    fi

}

export EDITOR="emacsclient -t"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm



# llvm require cmake, ninja
#

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[ -f /data/home/karlluo/news_spiders/scrapy/scrapy_crawl/bin/jin10_calendar/node_modules/tabtab/.completions/serverless.bash ] && . /data/home/karlluo/news_spiders/scrapy/scrapy_crawl/bin/jin10_calendar/node_modules/tabtab/.completions/serverless.bash
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
# uninstall by removing these lines or running `tabtab uninstall slss`
[ -f /data/home/karlluo/news_spiders/scrapy/scrapy_crawl/bin/jin10_calendar/node_modules/tabtab/.completions/slss.bash ] && . /data/home/karlluo/news_spiders/scrapy/scrapy_crawl/bin/jin10_calendar/node_modules/tabtab/.completions/slss.bash
export PYTHONDONTWRITEBYTECODE=x



# NPM packages in homedir
NPM_PACKAGES="$HOME/.npm-packages"

# Tell our environment about user-installed node tools
PATH="$NPM_PACKAGES/bin:$PATH"
# Unset manpath so we can inherit from /etc/manpath via the `manpath` command
unset MANPATH  # delete if you already modified MANPATH elsewhere in your configuration
MANPATH="$NPM_PACKAGES/share/man:$(manpath)"

# Tell Node about these packages
NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"


alias cnpm="npm --registry=https://registry.npm.taobao.org \
--cache=$HOME/.npm/.cache/cnpm \
--disturl=https://npm.taobao.org/dist \
--userconfig=$HOME/.cnpmrc"

function ArIpAddress() {
    local inter="http://members.3322.org/dyndns/getip"
    wget --quiet --no-check-certificate --output-document=- $inter
}

function ignore.io() { curl -sL https://www.gitignore.io/api/$@ >> .gitignore;}
function update.pb(){
    pb_file=~/doc_sc/srpc_proto/pb/news_subscribe.proto
    pb_name=news_subscribe
    service=subscribe_svr

    cd ~/srpc_lib
    ./create_rpc.sh  $pb_file ~/$service/$service_example --only-py
    cd ~/$service/$service_example_python/
    cp bin/$pb_name_pb2.py ../subscribe_svr_python/bin/$pb_name_pb2.py
    git commit -am "pb update"
    git push origin
}

function emacs.restart() {
    killall emacs
    emacs --daemon
}


function build_dir(){
    echo "hell"
}

function run.PbPress(){
    # 10.101.32.54 10.101.1.177
    cd ~/vv/comsearch-search/sugsvr/sug-video-music/rpc-press-test/
    git pull origin && blade build && ~/vv/build64_release/comsearch-search/sugsvr/sug-video-music/rpc-press-test/PbPress  --host=10.101.32.54 --filename=../server/config/music-sug.csv
}

function run.IndexBuilderTest(){
    cd ~/vv/comsearch-search/sugsvr/sug-video-music/segment-tree-index/test
    git pull origin && blade build && ~/vv/build64_release/comsearch-search/sugsvr/sug-video-music/segment-tree-index/test/IndexBuilderTest  ../../server/config/ 1
}

function run.IndexBuilder(){
    cd ~/vv/comsearch-search/sugsvr/sug-video-music/tools
    git pull origin && blade build && ~/vv/build64_release/comsearch-search/sugsvr/sug-video-music/tools/IndexBuilder  ../server/config/
    curl -s 'https://freeipa.vmic.xyz/api/file/upload/'  -H 'X-CSRFToken: O6sbGArEyL2BplTZ01RwsTqy2XvlOjVqScbtGvh0LT848UpVXSmVUyxl91snWna3' --cookie 'csrftoken=O6sbGArEyL2BplTZ01RwsTqy2XvlOjVqScbtGvh0LT848UpVXSmVUyxl91snWna3;sessionid=o9pp8cxvns1y3wlae4l86kx15k1rbpb9'  -X POST -F 'content_id=1340' -F file=@$HOME/vv/build64_release/comsearch-search/sugsvr/sug-video-music/tools/IndexBuilder
}

function run.DoubleBufferTest(){
    cd ~/vv/comsearch-search/sugsvr/sug-video-music/version-buffer
    git pull origin && blade build && ~/vv/build64_release/comsearch-search/sugsvr/sug-video-music/version-buffer/DoubleBufferTest  ../server/config/
}

function run.AcceptanceStandardTest(){
    resultfile=../../server/dict/result.csv
    cd ~/vv/comsearch-search/sugsvr/sug-video-music/segment-tree-index/test
    git pull origin && blade build && ~/vv/build64_release/comsearch-search/sugsvr/sug-video-music/segment-tree-index/test/AcceptanceStandardTest  --host=10.101.1.177 --queryfile=../../server/dict/query.csv --resultfile=$resultfile
    curl -s 'https://freeipa.vmic.xyz/api/file/upload/' -H 'X-CSRFToken: KBLBz74y7iyqwhWIVpTOULsIV9FhVfVahuDQFWoUhUEBZY9OPzVT9iwpQcjUcrbP' --cookie 'csrftoken=KBLBz74y7iyqwhWIVpTOULsIV9FhVfVahuDQFWoUhUEBZY9OPzVT9iwpQcjUcrbP;sessionid=4xl0no4zbize1scky7ql3ktp3b2et3f8' -X POST -F 'content_id=1340' -F file=@$resultfile
}

function run.loadServer(){
    cd ~/vv/comsearch-search/sugsvr/sug-video-music/server
    git pull origin && blade build && ~/vv/build64_release/comsearch-search/sugsvr/sug-video-music/server/videomusicsug  --config=config/config.xml &
}

function run.reload(){
    cd ~/vv/comsearch-search/sugsvr/sug-video-music/version-buffer
    git pull origin && blade build && ~/vv/build64_release/comsearch-search/sugsvr/sug-video-music/server/version-buffer/DoubleBufferTest  --config=config/config.xml &
}

function git.rebanch(){
    git checkout -b 11136502/video_music_sug origin/11136502/video_music_sug
}

function upload(){
    filename=$1
    curl -s 'https://freeipa.vmic.xyz/api/file/upload/' -H 'X-CSRFToken: qoRSTu2qF8M0iihEdsoxrmUiev2W5tWVPNxkGuVuFp1GBM2impAQWAwLI6wlUhnw' --cookie 'csrftoken=qoRSTu2qF8M0iihEdsoxrmUiev2W5tWVPNxkGuVuFp1GBM2impAQWAwLI6wlUhnw;sessionid=9d76cnzb1jdi5v99ga6vh67kjbolefvr' -X POST -F 'content_id=1340' -F file=@$filename
}


function uploadIndex(){
    cd ~/vv/comsearch-search/sugsvr/sug-video-music/server
    cp ~/vv/build64_release/comsearch-search/sugsvr/sug-video-music/server/videomusicsug .
    tar -zcvf 20210719.tar.gz config/20210719 videomusicsug
    file=20210719.tar.gz
    upload $file
}


function upload.big.directory(){
  dir=$1
  tar czf - $dir/ |split -b 1000m - $dir.tar.gz
  for filename in $(ls $dir.tar.gz*); do
      echo "File -> $f"
      xargs upload $filename
  done
}

function edit.bashrc(){
    emacsclient  -t ~/.bashrc && source ~/.bashrc
}


function test.rerun(){
     $* ;
     !-1 ;
}

function run.builder(){
    cd ~/vv/comsearch-search/sugsvr/sug-intelligent-life/tools
    git pull origin
    blade build
    cp ~/vv/build64_release/comsearch-search/sugsvr/sug-intelligent-life/tools/IndexBuilder .
    ./IndexBuilder
}

function run.test(){
    cd ~/vv/comsearch-search/sugsvr/sug-intelligent-life/
    git pull origin
    blade build
    cp ~/vv/build64_release/comsearch-search/sugsvr/sug-intelligent-life/SerializerTest .
    ./SerializerTest
}

PS1="\u@\h[\$(date +%k:%M:%S)] \W \$"
test -s ~/bin/bladefunctions && . ~/bin/bladefunctions || true
#scl enable devtoolset-7 bash

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$HOME/local/llvm/bin/:$PYENV_ROOT/bin:$PATH"
IpAddress=$(ArIpAddress)
if [ ${IpAddress} == "120.133.132.65" ]
then
    #120.133.132.65
    echo "hello laborer"
    CXX=$(which g++)
    CC=$(which gcc)
else
   eval "$(pyenv init --path)"
   #scl enable devtoolset-7 -- bash
   source scl_source enable devtoolset-8
   CXX=$HOME/local/gcc-8.5.0/bin/g++
   CC=$HOME/local/gcc-8.5.0/bin/gcc
fi

export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv virtualenv-init -)"

HOMEBREW_HOME=$HOME/.linuxbrew
export PKG_CONFIG_PATH=/usr/lib/pkgconfig:/usr/share/pkgconfig:$PKG_CONFIG_PATH
export PKG_CONFIG_PATH=$HOMEBREW_HOME/lib/pkgconfig:$PKG_CONFIG_PATH
export LD_LIBRARY_PATH=$HOMEBREW_HOME/lib:$LD_LIBRARY_PATH
export PATH=$PATH:$HOMEBREW_HOME/bin
